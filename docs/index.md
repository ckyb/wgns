---
title: WireGuard Namespace controller
hide:
  - navigation
---
# Overview
A namespaced WireGuard Interface Up+Down Controller

> :material-wifi-strength-alert-outline: Problems with packets falling out of your VPN when your WiFi connection bounces?  
> :fontawesome-solid-location-dot: Issues with WebRTC based geolocalization techniques bypassing your VPN?  
> :material-power: Expect your VPN to handle power management events and automatically resume?  
> :fontawesome-solid-faucet-drip: Tired of DNS leaks?  
> :material-pipe-wrench: Frustrated by `systemd` and NetworkManager GUI interference?  

`wgns` is designed to make it easy to use WireGuard to solve all these issues and more.

# Installation
## Requirements
``` bash
$ sudo apt install bash dhcpcd iproute2 wireguard-tools
```

## Source
``` bash
# sudo make install
```

## Packages
Releases are available for Ubuntu via the [PPA](https://launchpad.net/~hxr-io/+archive/ubuntu/aesthetic)

``` bash
$ sudo add-apt-repository ppa:hxr-io/aesthetic
$ sudo apt update
$ sudo apt install wgns
```

# Configue
[Configure :material-hammer-wrench:](config){ .md-button }

# Usage
## Startup
Bring up the default `wgvpn0` interface
``` bash
$ wgns up
```

## Shutdown
WireGuard interfaces can also be managed by the VPN interface name
``` bash
$ wgns down workvpn0
```


# Software Supply-chain
## License
All source code is dual-licensed under [MIT](https://spdx.org/licenses/MIT.html) or [Apache 2.0](https://spdx.org/licenses/Apache-2.0.html). You can choose between one of them if you use this work.  
All documentation is licensed under [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html).  
Some configuration and data files are licensed under [CC0-1.0](https://spdx.org/licenses/CC0-1.0.html).  

## Build targets
- Ubuntu :material-ubuntu: 22.04 (Jammy)
