## Settings

| Environmental Variable    | Default                                 | Description                 |
| ------------------------- | --------------------------------------- | --------------------------- |
| WGNS_WG_INTERFACE         | `wgvpn0`                                | WireGuard Network Interface |
| WGNS_CONFIG_ENV           | `~/.config/wgns/$WGNS_WG_INTERFACE.env` | `wgns` interface config     |
| WGNS_IP                   | *NONE*                                  | WireGuard local IP          |
| WGNS_DNS                  | *NONE*                                  | Domain name server          |
| WGNS_CONFIG_WPA           | `~/.config/wgns/wpa_supplicant.conf`    | WiFi Config                 |
| WGNS_CONFIG_WG_VPN        | `~/.config/wgns/wgvpn0.conf`            | WireGuard Config            |
| WGNS_NAMESPACE            | `wireguard`                             | WireGuard Namespace         |
| WGNS_INTERFACE            | `wlan0`                                 | Physical Network Interface  |
| WGNS_INTERFACE_TYPE       | `wifi`                                  | ['wifi','ethernet']         |
| WGNS_INTERFACE_PHY        | `phy0`                                  | WiFi PHY                    |
| WGNS_DHCP_CLIENT          | `dhcpcd`                                | ['dhcpcd','dhclient']       |
