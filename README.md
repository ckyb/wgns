# WireGuard namespace up/down controller
[![pipeline status](https://gitlab.com/hxr-io/wgns/badges/main/pipeline.svg)](https://gitlab.com/hxr-io/wgns/-/commits/main)
[![Latest Release](https://gitlab.com/hxr-io/wgns/-/badges/release.svg)](https://gitlab.com/hxr-io/wgns/-/releases)

-----

**Table of Contents**

* [Overview](#overview)
* [Installation](#installation)
* [Configuration](#configuration)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [License](#license)

## Overview

`wgns` is a command line tool for starting [WireGuard](https://www.wireguard.com/) in a kernel network namespace.

### Features
- Namespace prevents packets from leaking
- Prevent WebRTC based geolocalization techniques from bypassing VPN
- VPN connections persist after restoring from power suspend mode
- Can be used without uninstalling NetworkManager GUI

### Documentation
Additional documentation is available at https://hxr-io.gitlab.io/wgns/

### Build targets
- Ubuntu 22.04 (Jammy)

It should work on most systems that are modern enough to have WireGuard in the kernel.


## Installation
### Requirements
``` bash
$ sudo apt install bash dhcpcd iproute2 wireguard-tools
```

### Releases
Releases are available for Ubuntu via the [PPA](https://launchpad.net/~hxr-io/+archive/ubuntu/aesthetic)

``` bash
$ sudo add-apt-repository ppa:hxr-io/aesthetic
$ sudo apt update
$ sudo apt install wgns
```

or without `add-apt-repository`

``` bash
sudo gpg --homedir /tmp --no-default-keyring --keyring /usr/share/keyrings/hxr-io-ubuntu-aesthetic.gpg --keyserver keyserver.ubuntu.com --recv-keys AFD409CC9F89BC645881E432DB5A0AE1BEAFCC8A
echo "deb [signed-by=/usr/share/keyrings/hxr-io-ubuntu-aesthetic.gpg] https://ppa.launchpadcontent.net/hxr-io/aesthetic/ubuntu/ jammy main" | sudo tee /etc/apt/sources.list.d/hxr-io-ubuntu-aesthetic-jammy.list > /dev/null
sudo apt update
sudo apt install wgns
```


## Configuration
### Generate local keys
``` bash
RESET_UMASK=$(umask -p) \
&& umask 077 \
&& wg genkey | tee -a ~/.config/wgns/wgvpn0.key | wg pubkey > ~/.config/wgns/wgvpn0.pub \
&& $RESET_UMASK
```

### Config Files

#### `wgns`
Most configuration options can be passed via environmental variables. The variables configured in `$XDG_CONFIG_HOME/wgns/$WGNS_WG_INTERFACE.env` will be loaded automatically.

```
WGNS_INTERFACE=wlp2s0
WGNS_IP=172.16.15.14/12
```
*`~/.config/wgns/wgvpn0.env`*

#### WireGuard configuration
The WireGuard configuration is loaded via `wg setconf $WGNS_WG_INTERFACE ${WGNS_CONFIG_WG_VPN}` so any options that makes sense in that context should work.

```
################################
# Local
[Interface]
# Copy PrivateKey from ~/.config/wgns/wgvpn0.key
PrivateKey = gI6EdUSYvn8ugXOt8QQD6Yc+JyiZxIhp3GInSWRfWGE=

################################
# Upstream
[Peer]
PublicKey = HIgo9xNzJMWLKASShiTqIybxZ0U3wGLiUeJ1PKf8ykw=
AllowedIPs = 0.0.0.0/0 # Assign default route (Internet is upstream of peer)
Endpoint = 192.0.2.3:51820
```
*`~/.config/wgns/wgvpn0.conf`*

#### WiFi Configuration
An instance of `wpa_supplicant` is started in the network namespace for managing wifi connections.
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=adm
network={
    ssid="MyAccessPoint"
    id_str="myap"
    scan_ssid=0
    key_mgmt=WPA-PSK
    psk="A-super-sekret-password"
}
```
*`~/.config/wgns/wpa_supplicant.conf`*

## Usage
### TL;DR
``` bash
$ wgns up
$ traceroute www.example.com
$ wgns down
```

### Tethered via USB Ethernet device
``` bash
WGNS_INTERFACE=enp60s0u1u1 WGNS_INTERFACE_TYPE=ethernet ./wgns up
```

## Roadmap
- [ ] Support for default Ubuntu DHCP client

## Software Supply-chain
### License
All source code is dual-licensed under [MIT](https://spdx.org/licenses/MIT.html) or [Apache 2.0](https://spdx.org/licenses/Apache-2.0.html). You can choose between one of them if you use this work.
All documentation is licensed under [CC-BY-4.0](https://spdx.org/licenses/CC-BY-4.0.html).
Some configuration and data files are licensed under [CC0-1.0](https://spdx.org/licenses/CC0-1.0.html).
